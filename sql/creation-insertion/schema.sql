# https://dev.mysql.com/doc/refman/8.0/en/sql-syntax.html
# Voir Data Definition Statement

# https://dev.mysql.com/doc/refman/8.0/en/create-database.html
DROP SCHEMA annuaire;
CREATE SCHEMA annuaire;

USE annuaire;

# https://dev.mysql.com/doc/refman/8.0/en/create-table.html

CREATE TABLE annuaire.person (
	id INT AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL
);