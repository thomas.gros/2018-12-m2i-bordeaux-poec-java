CREATE DATABASE produits_premiers;

USE produits_premiers

DROP TABLE IF EXISTS representants;
CREATE TABLE representants
(num_representant CHAR(2) PRIMARY KEY,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50),
adresse CHAR(100),
ville VARCHAR(30),
etat VARCHAR(20),
code_postal CHAR(5),
total_commission DECIMAL(7,2),
taux_comission DECIMAL(3,2) );

DROP TABLE IF EXISTS clients;
CREATE TABLE clients
(numero_client CHAR(3) PRIMARY KEY,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
adresse CHAR(100),
ville VARCHAR(30),
etat VARCHAR(20),
code_postal CHAR(5),
solde DECIMAL(7,2),
limite_credit DECIMAL(6,2),
num_representant CHAR(2) );

DROP TABLE IF EXISTS commandes;
CREATE TABLE commandes
(numero_commande CHAR(5) PRIMARY KEY,
date_commande DATE,
numero_client CHAR(3) );

DROP TABLE IF EXISTS articles;
CREATE TABLE articles
(numero_article CHAR(4) PRIMARY KEY,
description TEXT,
stock_dispo DECIMAL(4,0),
categorie CHAR(2),
magasin CHAR(1),
prix_unitaire DECIMAL(6,2) );

DROP TABLE IF EXISTS lignes_commandes;
CREATE TABLE lignes_commandes
(numero_commande CHAR(5) NOT NULL,
numero_article CHAR(4) NOT NULL,
quantite_commande DECIMAL(3,0),
prix_facture DECIMAL(6,2),
PRIMARY KEY (numero_commande, numero_article) );
