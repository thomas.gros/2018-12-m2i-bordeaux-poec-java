USE produits_premiers;

# 1)	donner les numéro, nom, prénom et solde de chaque client 
# et classez le résultat par ordre de solde descendant.

SELECT numero_client, nom, prenom, solde
FROM clients
ORDER BY solde DESC;


# 2)	donner les numéro, nom, prénom et limite de crédit 
# de chaque client, classée par limite de crédit décroissante 
# et par nom dans la limite de crédit.
#  Les clients ayant la même limite de crédit sont classés 
# par ordre croissant.

SELECT numero_client, nom, prenom, limite_credit
FROM clients 
ORDER BY limite_credit DESC, nom;

# 3)	compter le nombre d’articles de la catégorie EM.
SELECT COUNT(*)
FROM articles
WHERE categorie = 'EM';

# 4)	trouver le nombre de clients total et leur solde total
SELECT COUNT(*) AS nb_clients, SUM(solde)
FROM clients;

# 5)	donner les numéros de clients qui ont une commande ouverte
# en ne listant chaque client qu’une fois.
SELECT DISTINCT numero_client
FROM commandes;

# 6)	donner la plus grande limite de crédit accordée 
# à un client du représentant 06.
SELECT MAX(limite_credit)
FROM clients
WHERE num_representant = '06';


# 7)	donner une colonne correspondant 
# à la concaténation du nom et du prénom pour tous les clients 
# dont le nom de famille n’est pas Adams.

SELECT CONCAT(nom, ' ', prenom)
FROM clients
WHERE nom != 'Adams';


# 8)	donner le montant des commandes de plus de 200$.
# lignes_commandes
SELECT SUM(prix_facture * quantite_commande)
FROM lignes_commandes
GROUP BY numero_commande
HAVING SUM(prix_facture * quantite_commande) > 200;

# 9)	donner les valeurs de la limite de crédit et le nombre de clients 
# pour chaque valeur, en ne listant 
# que les limites de crédit détenues par plus d’un seul client.

SELECT limite_credit, COUNT(*)
FROM clients
GROUP BY limite_credit
HAVING COUNT(*) > 1;

# 10) donner chaque limite de crédit et le nombre total de clients 
# du représentant n°3 ayant cette limite.

SELECT limite_credit, COUNT(*)
FROM clients
WHERE num_representant = 3
GROUP BY limite_credit;

# 11)	Donner la moyenne par ville des limites de crédit, 
# de solde pour les clients des représentants 06 et 03. 
# Les villes doivent être affichées en majuscule.
# Nommer les colonnes Ville, Moyenne_Limite_Credit 
# et Moyenne_Solde

SELECT UCASE(ville) as Ville, 
		      AVG(limite_credit) as Moyenne_Limite_Credit,
              AVG(solde) as Moyenne_Solde
FROM clients
WHERE num_representant IN ('03', '06')
GROUP BY ville;     

              
              
              
              

















































