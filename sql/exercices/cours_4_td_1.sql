USE produits_premiers;

# 1)	Donner les numéro, nom et prénom de chaque client 
# ainsi que les numéro, nom et prénom de leur représentant

SELECT clients.numero_client, clients.nom, clients.prenom,
			   representants.num_representant, representants.nom, representants.prenom
FROM clients
	INNER JOIN representants
		ON clients.num_representant = representants.num_representant;

# 2)	Idem que 1) mais pour les clients dont la limite de crédit 
# est de 1000$
SELECT clients.numero_client, clients.nom, clients.prenom,
			   representants.num_representant, representants.nom, representants.prenom
FROM clients
	INNER JOIN representants
		ON clients.num_representant = representants.num_representant
		AND clients.limite_credit = 1000;
# WHERE clients.limite_credit = 1000;

# 3)	Pour chaque article de la commande, 
# donner les numéros de commande et d’article, 
# ainsi que la description, la quantité commandée, 
# le prix facturé et le prix unitaire.

SELECT commandes.numero_commande,
			   lignes_commandes.numero_article,
               articles.description,
               lignes_commandes.quantite_commande,
               lignes_commandes.prix_facture,
               articles.prix_unitaire
FROM
	commandes 
    INNER JOIN lignes_commandes
		ON commandes.numero_commande = lignes_commandes.numero_commande
	INNER JOIN articles
		ON lignes_commandes.numero_article = articles.numero_article;


# 4)	Donnez le numéro de client, 
# le numéro et la date de commande 
# ainsi que le montant total de la commande 
# pour chaque commande dont le montant est supérieur à 100$

SELECT 
	commandes.numero_client,
    commandes.numero_commande,
    commandes.date_commande,
    SUM(lignes_commandes.quantite_commande * lignes_commandes.prix_facture)
FROM  commandes
	INNER JOIN lignes_commandes
		ON commandes.numero_commande = lignes_commandes.numero_commande
GROUP BY commandes.numero_commande, 
				    commandes.date_commande, 
					commandes.numero_client
HAVING SUM(lignes_commandes.quantite_commande * lignes_commandes.prix_facture) > 100;

# 5)	Trouver chaque paire de client ayant 
# un nom et un prénom identiques.

SELECT C1.numero_client, C1.nom, C1.prenom, 
			   C2.numero_client, C2.nom, C2.prenom
FROM clients AS C1
	INNER JOIN clients AS C2
		ON C1.nom = C2.nom
        AND C1.prenom = C2.prenom
        AND C1.numero_client > C2.numero_client;

# 6)	Donner les noms et prénoms des clients 
# ainsi que les articles qu’ils ont respectivement commandé. 
# Si un client n’a pas commandé d’article 
# il doit quand même apparaître comme client 
# n’ayant pas commandé d’article.

SELECT clients.nom, clients.prenom,
				lignes_commandes.numero_article
FROM clients
	LEFT OUTER JOIN commandes
		ON clients.numero_client = commandes.numero_client
	LEFT OUTER JOIN lignes_commandes
		ON commandes.numero_commande = lignes_commandes.numero_commande;
                
-- FROM lignes_commandes 
-- 	INNER JOIN commandes
-- 		ON lignes_commandes.numero_commande = commandes.numero_commande
-- 	RIGHT OUTER JOIN clients
-- 		ON clients.numero_client = commandes.numero_client

# 7)	Donner pour chaque article d’une commande : 
# le numéro d’article, la quantité commandée, le numéro 
# et la date de commande, les numéro, nom, prénom du client 
# qui a passé la commande 
# ainsi que les nom et prénom du représentant de ce client.
SELECT LC.numero_article, LC.quantite_commande,
			   C.numero_commande, C.date_commande,
               CL.numero_client, CL.nom, CL.prenom,
               R.nom, R.prenom
FROM lignes_commandes AS LC
	INNER JOIN commandes AS C
		ON LC.numero_commande = C.numero_commande
	INNER JOIN clients AS CL
		ON C.numero_client = CL.numero_client
	INNER JOIN representants AS R
		ON CL.num_representant = R.num_representant;
               
			   