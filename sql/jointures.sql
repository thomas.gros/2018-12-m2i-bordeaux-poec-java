# les clients et leur adresse
SELECT customer.first_name, customer.last_name, address.address
FROM customer INNER JOIN address
	ON customer.address_id = address.address_id;
    
# titre du film, language
SELECT film.title, language.name
FROM film INNER JOIN language
	ON film.language_id = language.language_id;

# firstname (actor), last_name(actor), film_id dans lequel il a joué

SELECT actor.first_name, actor.last_name, film_actor.film_id
FROM actor INNER JOIN film_actor
	ON actor.actor_id = film_actor.actor_id;


# firstname (actor), last_name(actor), film_id 
# dans lequel l'acteur d'id 1 a joué
SELECT actor.first_name, actor.last_name, film_actor.film_id
FROM actor INNER JOIN film_actor
	ON actor.actor_id = film_actor.actor_id
WHERE actor.actor_id = 1;

SELECT actor.first_name, actor.last_name, film.title
FROM actor 
			INNER JOIN film_actor
					ON actor.actor_id = film_actor.actor_id
						AND actor.actor_id = 1
			INNER JOIN film
					ON film_actor.film_id = film.film_id;
                    

SELECT A.first_name, A.last_name, F.title
FROM actor AS A
			INNER JOIN film_actor AS FA
					ON A.actor_id = FA.actor_id
						AND A.actor_id = 1
			INNER JOIN film AS F
					ON FA.film_id = F.film_id;

# customer.first_name, customer.last_name, country.country

SELECT customer.first_name, customer.last_name, country.country
FROM customer
	INNER JOIN address
		ON customer.address_id = address.address_id
	INNER JOIN city
		ON address.city_id = city.city_id
	INNER JOIN country
		ON city.country_id = country.country_id;

# customer_id, customer.first_name, customer.last_name, film.title
# pour chaque film loué par chaque customer

SELECT customer.customer_id, customer.first_name, customer.last_name,
			   film.title
FROM customer
	INNER JOIN rental
		ON customer.customer_id = rental.customer_id
	INNER JOIN inventory
		ON rental.inventory_id = inventory.inventory_id
	INNER JOIN film
		ON inventory.film_id = film.film_id;

# Quels sont clients qui ont loué ACADEMY DINOSAUR ?

SELECT customer.customer_id, customer.first_name, customer.last_name,
			   film.title
FROM customer
	INNER JOIN rental
		ON customer.customer_id = rental.customer_id
	INNER JOIN inventory
		ON rental.inventory_id = inventory.inventory_id
	INNER JOIN film
		ON inventory.film_id = film.film_id
        AND film.title = 'ACADEMY DINOSAUR';


# Acteurs n'ayant joué dans aucun films
SELECT actor.actor_id, film_actor.film_id
FROM actor 
	LEFT OUTER JOIN film_actor
		ON actor.actor_id = film_actor.actor_id
WHERE film_actor.film_id IS NULL;

# Acteurs n'ayant pas joué dans le film d'id 1
SELECT actor.*, film_actor.*
FROM actor 
	LEFT OUTER JOIN film_actor
		ON actor.actor_id = film_actor.actor_id
        AND film_actor.film_id = 1
WHERE film_actor.actor_id IS NULL;

# jointure d'une table avec elle même = auto jointure 
# pas de condition de jointure => CROSS JOIN
SELECT A1.*, A2.*
FROM actor AS A1 
	INNER JOIN actor AS A2; 

SELECT A1.*, A2.*
FROM actor AS A1 
	CROSS JOIN actor AS A2;







