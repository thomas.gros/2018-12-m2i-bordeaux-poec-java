USE sakila;

SELECT first_name
FROM actor;

SELECT first_name, last_name
FROM actor;

SELECT first_name, last_name, actor_id
FROM actor;

# * = toutes les colonnes
SELECT *
FROM actor;

SELECT *
FROM actor
WHERE first_name = 'PENELOPE';

SELECT *
FROM actor
WHERE first_name = 'PENELOPE' AND actor_id > 100;

# EXERCICE 
# Selectionner le titre de tous les films dont la durée dépasse 1h
SELECT title
FROM film
WHERE length > 60;

# Selectionner l'id et le titre de tous les films dont la catégorie est PG-13
# ET dont la durée dépasse 1h
SELECT film_id, title
FROM film
WHERE rating = 'PG-13' AND length > 60;

# Selectionner tous les clients qui ne sont pas client du magasin 2

SELECT *
FROM customer
WHERE store_id != 2;

#Selectionner tous les films dont la durée est comprise entre 50 minutes
# (inclus) et 1h30 (exclu)
SELECT *
FROM film
WHERE length >= 50 AND length < 90;

# Tous les films PG-13 et NC-17
SELECT *
FROM film
WHERE rating IN ('PG-13', 'NC-17');

SELECT *
FROM film
WHERE rating = 'PG-13' OR rating = 'NC-17';

# Tous les films durant entre 1h et 1h30
SELECT *
FROM film
WHERE length BETWEEN 60 AND 90;

SELECT *
FROM film
WHERE length >= 60 AND length <= 90;

# tous les clients dont le prénom commence par PE
SELECT *
FROM customer
WHERE first_name LIKE 'PE%';

# tous les clients dont le prénom termine par A
SELECT *
FROM customer
WHERE first_name LIKE '%A';

# tous les clients dont le prénom contient AS
SELECT *
FROM customer
WHERE first_name LIKE '%AS%';

# tous les clients dont le prénom a pour deuxième lettre A
SELECT *
FROM customer
WHERE first_name LIKE '_A%';

# tous les clients dont le prénom a pour avant denrière lettre A
SELECT *
FROM customer
WHERE first_name LIKE '%A_';


# 1)	donner les numéros, nom, prénom de chaque acteur
SELECT actor_id, last_name, first_name
FROM actor;

# 2) lister la table pays complète
SELECT  *
FROM country;

# 3) donner le nom du pays dont l id est 5
SELECT country
FROM country
WHERE country_id = 5;

# 4) donner le numéro de chaque client dont le prenom est Thomas
SELECT customer_id
FROM customer
WHERE first_name = 'Thomas';

# 5) donner le nom_complet de chaque client

SELECT CONCAT(first_name, ' ', last_name)
FROM customer;

SELECT CONCAT(first_name, ' ', last_name), 1+1
FROM customer;

# Titre de chaque film et la durée du film exprimée en heures
SELECT title, length / 60 AS duration_in_hour
FROM film;

# Afficher le prénom et la longueur du prénom pour chaque acteur
# CHAR_LENGTH()
SELECT first_name, CHAR_LENGTH(first_name)
FROM actor;

# Afficher les acteurs (*) dont le prénom contient >= 6 caractères
SELECT *
FROM actor
WHERE CHAR_LENGTH(first_name) >= 6;


SELECT title, 
			  CASE  WHEN length > 90 THEN 'long'  
						  WHEN length BETWEEN 60 AND 90 THEN 'moyen'
					   # WHEN length >= 60 AND length <= 90  THEN 'moyen'
						  ELSE 'court'
			  END AS duree
FROM film;


# Fonctions d'aggrégation
# durée moyenne et durée cumulée des films PG-13
SELECT AVG(length), SUM(length), MIN(length), MAX(length)
FROM film
WHERE rating = 'PG-13';

SELECT AVG(CHAR_LENGTH(first_name))
FROM actor;

SELECT COUNT(*)
FROM actor;

SELECT COUNT(*)
FROM film;

SELECT COUNT(length) # excepté valeurs nulles
FROM film;

SELECT COUNT(DISTINCT length) # sans les doublons
FROM film;

SELECT DISTINCT length
FROM film;

SELECT DISTINCT first_name
FROM actor;

# GROUP BY

SELECT rating, AVG(length)
FROM film
GROUP BY rating;

# Calculer le nombre de client par magasin
# store_id, number_of_client

# SELECT store_id, COUNT(*), JSON_ARRAYAGG(email)
SELECT store_id, COUNT(*), GROUP_CONCAT(email)
FROM customer
GROUP BY store_id;


# HAVING
SELECT rating, COUNT(*)
FROM film
GROUP BY rating
HAVING COUNT(*) > 200;

SELECT rating, COUNT(*)
FROM film
WHERE length < 150
GROUP BY rating
HAVING COUNT(*) > 160;

# Pour tous les prénoms, combien d'acteurs ont le même prénom
SELECT first_name, COUNT(*)
FROM actor
GROUP BY first_name;

# ORDER BY
SELECT *
FROM actor
# ORDER BY first_name ASC;
# ORDER BY first_name DESC;
# ORDER BY actor_id DESC; 
ORDER BY first_name DESC, last_name DESC;

SELECT first_name, COUNT(*)
FROM actor
GROUP BY first_name
ORDER BY COUNT(*) DESC;

SELECT first_name, COUNT(*) as total
FROM actor
GROUP BY first_name
ORDER BY total DESC;































