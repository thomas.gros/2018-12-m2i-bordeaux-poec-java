// document.querySelector('body')

const button = document.createElement('button');
button.appendChild(document.createTextNode('fullscreen'));
document.body.prepend(button);

button.addEventListener('click', (e) => {
    document.querySelector('#mapid')
            .requestFullscreen();
})


var mymap = L
                .map('mapid')
                .setView([51.505, -0.09], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'your.token.here'
}).addTo(mymap);

// https://w3c.github.io/geolocation-api/
// à surveiller: https://w3c.github.io/geolocation-sensor/


function showMap(position) {
    // Show a map centered at (position.coords.latitude, position.coords.longitude).
    console.log(position);
    mymap.setView([position.coords.latitude, 
                   position.coords.longitude], 15);
}
  
// One-shot position request.
navigator.geolocation.getCurrentPosition(showMap);