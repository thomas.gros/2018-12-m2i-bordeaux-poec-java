class LinkComponent {
    constructor() {
        this._root = document.createElement('a');
        this._root.appendChild(document.createTextNode(''));
        this._root.setAttribute('href', '');
    }

    get root() {
        return this._root;
    }

    set text(nextText) {
        this._root.firstChild.nodeValue = nextText;
    }

    set onclick(cb) {
        this._root.addEventListener('click', cb);
    }

    render() {
    }
}