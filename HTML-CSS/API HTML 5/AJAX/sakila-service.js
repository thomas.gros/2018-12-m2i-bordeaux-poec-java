class SakilaService {

    findFilms(from, count, callback) {
       // callback('coucou');

        // on dirait que on a fait la requete HTTP
        //let films = [];
        // setTimeout(function() {
        //     callback(films);
        // }, 1000);

        const xhr = new XMLHttpRequest();
        xhr.open('GET', 
                `http://localhost:8080/servlet-0.0.1-SNAPSHOT/api/films?from=${from}&count=${count}`);
        xhr.onreadystatechange = function(e) {
            // XMLHttpRequest.DONE = 4
            if(xhr.readyState == XMLHttpRequest.DONE) {         
                const data = JSON.parse(xhr.responseText);
                callback(data);
            }
        }
        xhr.send(null);
    }
}