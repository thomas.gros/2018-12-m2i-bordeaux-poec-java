class FilmsListComponent {

    constructor() {
        this._root = document.createElement('div');
        this._films = [];
    }

    get root() {
        return this._root;
    }

    set films(newFilms) {
        this._films = newFilms;
        this.render();
    }

    render() {
        while (this._root.firstChild) {
            this._root.removeChild(this._root.firstChild);
        }

        let html = `
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>title</th>
                    </tr>
                </thead>
                <tbody>`;

        for(const f of this._films) {        
            html += `<tr>
                        <td>${f.film_id}</td>
                        <td>${f.title}</td>
                    </tr>`;
        }

        html += `</tbody></table>`;

        this._root.innerHTML = html;
        
            // this._root.appendChild(
            //     document.createTextNode(this._films)
            // );
    }
}