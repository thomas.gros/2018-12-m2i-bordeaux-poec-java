const sakilaService = new SakilaService();

const filmsList = new FilmsListComponent();
document.querySelector('body').prepend(filmsList.root);

let from = 0;
let count = 10;

// setInterval(function() {
//     sakilaService.findFilms(from, count, function(films) {
//         filmsList.films = films;
//         from += count;
//         if(from  > 1000) { from = 0}
//     });
// }, 3000);

const nextLink = new LinkComponent();

nextLink.text = 'next';

nextLink.onclick = function(e) {
    e.preventDefault();
    sakilaService.findFilms(from, count, function(films) {
        filmsList.films = films;
        from += count;
        if(from  > 1000) { from = 0}
    });
}

document.querySelector('body').prepend(nextLink.root);