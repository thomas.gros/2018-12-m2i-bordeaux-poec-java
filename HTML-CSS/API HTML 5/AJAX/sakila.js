let state = {
    from: 0,
    count: 10
};

const xhr = new XMLHttpRequest();
xhr.open('GET', 
         `http://localhost:8080/servlet-0.0.1-SNAPSHOT/api/films?from=${state.from}&count=${state.count}`);
xhr.onreadystatechange = function(e) {
    // XMLHttpRequest.DONE = 4
    if(xhr.readyState == XMLHttpRequest.DONE) {         
        const data = JSON.parse(xhr.responseText);
        console.log(data);
        console.log(data[0]);
        console.log(data[0].film_id);
        document.querySelector('body')
                .innerHTML = filmsToHTML(data);
    }
}
xhr.send(null);

function filmsToHTML(films) {
    let html = '';
    html += '<table>';
    html += '<thead><tr><th>id</th><th>title</th></tr></thead>';
    html += '<tbody>';
    for (const f of films) {
        html += `<tr>
                    <td>${f.film_id}</td>
                    <td>${f.title}</td>
                </tr>`;
    }
    html += '</tbody>';
    html += '</table>';
    html+='<a href="'+ `http://localhost:8080/servlet-0.0.1-SNAPSHOT/api/films?from=${state.from}&count=${state.count}` + '">next</a>';
    return html;
}