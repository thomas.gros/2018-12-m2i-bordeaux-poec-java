const svg = document
                .createElementNS('http://www.w3.org/2000/svg',
                'svg');
svg.setAttributeNS(null, 'width', 300);
svg.setAttributeNS(null, 'height', 150);
svg.setAttributeNS(null, 
                  'transform',
                  'matrix(1,0,0,-1,0,0)');

document.querySelector('#svg-area')
        .appendChild(svg);

const data = [1,2,3,4,5];

const spaceBetweenBars = 20;
const barWidth = 10;
let currentX = 0;
let max = Math.max(...data);

data.forEach(e => {
    const bar = document
                .createElementNS('http://www.w3.org/2000/svg',
                                 'rect');
    bar.setAttributeNS(null, 'x', currentX);
    bar.setAttributeNS(null, 'y', 0);
    bar.setAttributeNS(null, 'width', barWidth);
    bar.setAttributeNS(null, 'height', e * svg.clientHeight / max);
    bar.setAttributeNS(null, 'fill', 'blue');
    svg.appendChild(bar);

    currentX += spaceBetweenBars + barWidth;
});

