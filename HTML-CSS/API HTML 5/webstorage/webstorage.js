// https://html.spec.whatwg.org/multipage/#toc-webstorage

// if(! sessionStorage.getItem('data')) {
//     sessionStorage.setItem('data', 'hello');
//     console.log('stored data');
// } else {
//     console.log('data already exist');
//     console.log(sessionStorage.getItem('data'));
// }

if(! localStorage.getItem('data')) {
    localStorage.setItem('data', 'hello');
    console.log('stored data');
} else {
    console.log('data already exist');
    console.log(localStorage.getItem('data'));
}