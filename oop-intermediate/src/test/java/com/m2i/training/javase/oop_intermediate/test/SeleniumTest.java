package com.m2i.training.javase.oop_intermediate.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * https://sites.google.com/a/chromium.org/chromedriver/getting-started
 */
class SeleniumTest {

	@Test
	@Disabled
	public void testGoogleSearch() throws InterruptedException {
	  // Optional, if not specified, WebDriver will search your path for chromedriver.
	  System.setProperty("webdriver.chrome.driver", 
			  			 "/Users/thomasgros/Downloads/chromedriver");

	  WebDriver driver = new ChromeDriver();
	  driver.get("http://www.google.com/xhtml");
	  assertEquals("Google", driver.getTitle());
	  
	  // Thread.sleep(5000);
	  // Let the user actually see something!
	  WebElement searchBox = driver.findElement(By.name("q"));
	  searchBox.sendKeys("ChromeDriver");
	  searchBox.submit();
	  // Thread.sleep(5000);
	  
	  // Let the user actually see something!
	  driver.quit();
	}

	@Test
	public void testExampleDotComSearch() {
		System.setProperty("webdriver.chrome.driver", 
	  			 		   "/Users/thomasgros/Downloads/chromedriver");

		WebDriver driver = new ChromeDriver();
		driver.get("http://www.example.com");
		assertEquals("Example Domain", driver.getTitle());
		
		WebElement h1 = driver.findElement(By.tagName("h1"));
		assertEquals("Example Domain", h1.getText());
		
		WebElement a = driver.findElement(By.tagName("a"));
		assertEquals("More information...", a.getText());
		
		driver.quit();
		
	}
	
	
	
	
	
	
	
	
	
	
	
}
