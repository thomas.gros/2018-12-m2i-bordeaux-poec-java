package com.m2i.training.javase.oop_intermediate.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class HelloTest {

	@Test
	void testSayHello() {
		// Arranger son test
		Hello hello = new Hello();
		
		// Agir
		String result = hello.sayHello();
		
		// Assertions
		assertEquals("hello", result);
	}
	
	@Test
	public void testSayHelloWithName() {
		// Arrange
		Hello hello = new Hello();
		
		// Act
		String result = hello.sayHello("Thomas");
		
		// Assert
		assertEquals("hello Thomas", result);	
	}
	
	@Test
	public void testSayHelloToNull() {
		Hello hello = new Hello();
		
		String result = hello.sayHello(null);
		
		assertEquals("hello", result);
	}
	
	
	
	
	
	
	
	
	

}