package com.m2i.training.javase.oop_intermediate.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class CalculatorTest {
// string, char, ...
	
	@Test
	void testAdd() {
		Calculator calculator = new Calculator();
		int a = 2;
		int b = 4;
		
		int result = calculator.add(a, b);
		
		assertEquals(6, result);
	}
	
	@Test
	void testOnePositiveAndOneNegative() {
		Calculator calculator = new Calculator();
		int a = -10;
		int b = 5;
		
		int result = calculator.add(a, b);
		
		assertEquals(-5, result);
	}
	
	@Test
	void testTwoBigNumbers() {
		Calculator calculator = new Calculator();
		int a = 1;
		int b = Integer.MAX_VALUE;
		
		try {
			int result = calculator.add(a, b);
		} catch(IllegalArgumentException ex) {
			return;
		}
		
		fail("pas d'exception");
	}
	
	@Test
	void testTwoSmallNumbers() {
		Calculator calculator = new Calculator();
		int a = -1;
		int b = Integer.MIN_VALUE;
		
		try {
			int result = calculator.add(a, b);
		} catch(IllegalArgumentException ex) {
			return;
		}
		
		fail("pas d'exception");
	}
	@Test
	void testTwoMaxValue() {
		Calculator calculator = new Calculator();
		int a = Integer.MAX_VALUE;
		int b = Integer.MAX_VALUE;
		
		try {
			int result = calculator.add(a, b);
		} catch(IllegalArgumentException ex) {
			return;
		}
		
		fail("pas d'exception");
	}
	
//	
//	@Test
//	void testTwoMinValue() {
//		Calculator calculator = new Calculator();
//		int a = Integer.MIN_VALUE;
//		int b = Integer.MIN_VALUE;
//		
//		try {
//			int result = calculator.add(a, b);
//		} catch(IllegalArgumentException ex) {
//			return;
//		}
//		
//		fail("pas d'exception");
//	}
	
	
	

}
