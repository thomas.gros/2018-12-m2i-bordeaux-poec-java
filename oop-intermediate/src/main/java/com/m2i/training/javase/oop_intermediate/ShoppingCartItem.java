package com.m2i.training.javase.oop_intermediate;

public class ShoppingCartItem {

	private Book book;
	private int qty;
	
	public ShoppingCartItem(Book book, int qty) {
		this.book = book;
		this.qty = qty;
	}

	public Book getBook() {
		return book;
	}
	
	public int getQty() {
		return qty;
	}

}
