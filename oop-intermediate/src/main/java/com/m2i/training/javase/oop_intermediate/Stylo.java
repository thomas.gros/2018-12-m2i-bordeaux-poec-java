package com.m2i.training.javase.oop_intermediate;

public class Stylo {

	private Bouchon bouchon;
	
	public Bouchon getBouchon() {
		return bouchon;
	}
	
	public void setBouchon(Bouchon bouchon) {
		this.bouchon = bouchon;
	}
	
}
