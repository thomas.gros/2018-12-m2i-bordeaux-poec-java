package com.m2i.training.javase.oop_intermediate;

public class SomeClassMain {

	public static void main(String[] args) {
		SomeClass instance = new SomeClass();

		// Class<? extends SomeClass> clazz = instance.getClass();
		Class clazz = instance.getClass(); // reflection
		System.out.println(clazz.getName());
		
		
		System.out.println(instance.equals(instance));
		System.out.println(Integer.toHexString(instance.hashCode()));
		

		SomeClass instance1 = new SomeClass();
		SomeClass instance2 = new SomeClass();
		
		System.out.println(instance1 == instance2);
		
		SomeClass instance3 = instance1;
		System.out.println(instance3 == instance1);

		System.out.println(instance1.equals(instance2));
		System.out.println(instance1.hashCode());
		System.out.println(instance2.hashCode());
		// instance2.setAge(12);
		// System.out.println(instance1.equals(instance2));
	}

}
