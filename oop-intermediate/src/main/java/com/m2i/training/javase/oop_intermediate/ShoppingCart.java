package com.m2i.training.javase.oop_intermediate;

public class ShoppingCart {

	private ShoppingCartItem[] items;
	private int qtyOfItemsInCart;
	
	public ShoppingCart() {
		items = new ShoppingCartItem[30];
		qtyOfItemsInCart = 0;
	}

	public void add(Book book, int qty) {
		// qté < 0 ? book null ?, ...
		items[qtyOfItemsInCart] = new ShoppingCartItem(book, qty);
		qtyOfItemsInCart++;
	}

	public ShoppingCartItem[] getItems() {
		return items;
	}

}
