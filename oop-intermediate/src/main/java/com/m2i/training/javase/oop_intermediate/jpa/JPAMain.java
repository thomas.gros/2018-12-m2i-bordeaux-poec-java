package com.m2i.training.javase.oop_intermediate.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.m2i.training.javase.oop_intermediate.hibernate.Film;

public class JPAMain {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("pu1");
		
		EntityManager entityManager = 
				entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();

		Film f = entityManager.find(Film.class, new Long(1));
		System.out.println(f);
		entityManager.getTransaction().commit();

		entityManager.close();
		
		entityManagerFactory.close();
		
	}

}
