package com.m2i.training.javase.oop_intermediate.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.m2i.training.javase.oop_intermediate.hibernate.Film;

public class JPQLMain {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("pu1");
		
		EntityManager entityManager = 
				entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();

		 // ATTENTION CE N'EST PAS DU SQL
		
		// SELECTIONNER TOUS LES FILMS
		String qlString = "SELECT f FROM Film f";

		TypedQuery<Film> query = 
				entityManager.createQuery(qlString, Film.class);
		query.setFirstResult(10);
		query.setMaxResults(50);
		List<Film> films = query.getResultList();

		// SELECTIONNER TOUS LES FILMS DONT l'id < 500
		qlString = "SELECT f FROM Film f WHERE f.id < 500";

		query = 
				entityManager.createQuery(qlString, Film.class);
		
		films = query.getResultList();
		
		// SELECTIONNER TOUS LES FILMS DONT l'id < data
		qlString = "SELECT f FROM Film f WHERE f.id < :data";

		query = 
				entityManager.createQuery(qlString, Film.class);
			
		query.setParameter("data", new Long(150));
		
		films = query.getResultList();
		

		// SELECTIONNER LE FILM d'ID 1
		qlString = "SELECT f FROM Film f WHERE f.id = :data";

		query = 
				entityManager.createQuery(qlString, Film.class);
			
		query.setParameter("data", new Long(1));
		
		Film f1 = query.getSingleResult();
		
		
		// Jointures
		// N+1 SELECT problem
		// qlString = "SELECT f FROM Film f JOIN f.actors a ON a.id = 1";
		qlString = "SELECT f FROM Film f JOIN FETCH f.actors a";
		query = 
				entityManager.createQuery(qlString, Film.class);
		films = query.getResultList();
		
		for(Film f: films) {
			System.out.println(f.getActors());
		}
		//System.out.println(films);
		
		
		entityManager.getTransaction().commit();

		entityManager.close();
		
		entityManagerFactory.close();

	}

}
