package com.m2i.training.javase.oop_intermediate;

public class Dog extends Animal {

	public Dog(String name) {
		super(name);
	}

	@Override
	public void makeSound() {
		System.out.println("woof");
	}
	
	
}
