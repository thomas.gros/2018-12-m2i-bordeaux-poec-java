package com.m2i.training.javase.oop_intermediate;

public class AnimalMain {

	public static void main(String[] args) {
		Cat cat = new Cat("fido");
		Dog dog = new Dog("medor");
		
		System.out.println(cat.getName());
		System.out.println(dog.getName());
		
		System.out.println(cat.getAge());
		
		dog.makeSound();
		cat.makeSound();
		
		Animal c2 = new Cat("felix");
		c2.makeSound(); // meow

		Object c3 = new Cat("tom");

		// c3.makeSound(); // ne compile pas
		System.out.println(c3 instanceof Object);
		System.out.println(c3 instanceof Animal);
		System.out.println(c3 instanceof Cat);
		System.out.println(c3 instanceof Dog);
		
		if(c3 instanceof Cat) { // obligation morale :)
			Cat c4 = (Cat) c3; // cast / transtypage
			c4.makeSound();
		}
		
		// Object c5 = new Dog("woof");
		// Cat c6 = (Cat) c5; // attention, ClassCastException
		
//		Animal[] tabA = new Animal[10];
//		tabA[0] = c2;
//		tabA[1] = cat;
//		tabA[2] = dog;
//		
//		Cat[] tabB = new Cat[10];
//		tabB[0] = cat;
//		tabB[1] = c2; // ne compile pas
		
		
//		Animal a = new Animal("hello"); // abstract = on ne peut pas instancier la classe
		
	}

}
