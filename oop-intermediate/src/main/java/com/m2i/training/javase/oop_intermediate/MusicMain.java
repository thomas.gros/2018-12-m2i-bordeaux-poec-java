package com.m2i.training.javase.oop_intermediate;

public class MusicMain {

	public static void main(String[] args) {
		Song s1 = new Song("Petit papa noël", 180);
//		Song s2 = new Song("nkjnjkj", -25);
		Song s3 = new Song("Jingle Bells", 160);
		
		Playlist playlist = new Playlist();
		playlist.addSong(s1);
		playlist.addSong(s3);
		
		playlist.addSong(null);	
	}

}
