package com.m2i.training.javase.oop_intermediate;

public class PersonMain {

	public static void main(String[] args) {
		Person p = new Person("Thomas", "Gros");

		p = null;

		System.out.println(Person.DEFAULT_EMAIL);

		System.out.println(p.DEFAULT_EMAIL); // à éviter

		final int i = 0;
		// i = 4; // erreur de compilation
		
		
	}

}
