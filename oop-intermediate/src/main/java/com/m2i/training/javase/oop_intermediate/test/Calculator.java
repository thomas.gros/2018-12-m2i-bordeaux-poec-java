package com.m2i.training.javase.oop_intermediate.test;

public class Calculator {

	/**
	 * Additionne deux nombres.
	 * @param a
	 * @param b
	 * @return la somme des nombres
	 * @throws IllegalArgumentException si a + b n'est pas dans
	 * l'intervalle [Integer.MIN_VALUE, Integer.MAX_VALUE]
	 */
	public int add(int a, int b) {
		int result = a + b;
		
		if (a > 0 && b > 0 && result < 0) {
			throw new IllegalArgumentException("a + b > INTEGER.MAX_VALUE");
		}
		
		if(a < 0 && b < 0 && result >= 0) {
			throw new IllegalArgumentException("a + b < INTEGER.MIN_VALUE");
		}
		
		return result;
	}
	
}
