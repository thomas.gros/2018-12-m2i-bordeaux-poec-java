package com.m2i.training.javase.oop_intermediate;

public class Book {
	private String title;

	public Book(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}	
}
