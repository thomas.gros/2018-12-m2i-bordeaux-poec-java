package com.m2i.training.javase.oop_intermediate;

public class EcommerceMain {

	public static void main(String[] args) {

		// On veut pouvoir acheter des livres
		
		// 1) Comment représenter la notion de Livre ?
		// Book book = new Book("Java c'est bien");
		
		// on définit le titre au moment de l'instanciation
		Book book = new Book("Java c'est bien");
		Book book2 = new Book("Java 8 c'est bien");
		
		// 2) Comment ajouter des livres 
		//    dans un panier d'achat ?
		ShoppingCart cart = new ShoppingCart();
		cart.add(book, 1);
		cart.add(book2, 5);
		
		// 3) Comment lister les livres disponibles 
		//    dans un panier d'achat ?
		ShoppingCartItem[] items = cart.getItems();
		for(int i = 0; i < items.length; i++) {
			if(items[i] != null) {
				System.out.println(items[i].getBook().getTitle() 
							 	   + " - " + items[i].getQty());
			}
		}

		// 4) Comment gérer la notion de quantité d'un 
		//    livre dans un panier d'achat ?
		// cf 2, directement fait en un coup
	}

}
