package com.m2i.training.javase.oop_intermediate.sakila;

public class Country {

	private String countryId;
	private String country;
	
	public Country(String countryId, String country) {
		super();
		this.countryId = countryId;
		this.country = country;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Country [countryId=" + countryId + ", country=" + country + "]";
	}
}
