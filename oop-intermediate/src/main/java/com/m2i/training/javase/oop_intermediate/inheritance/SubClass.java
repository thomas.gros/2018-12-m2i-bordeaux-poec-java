package com.m2i.training.javase.oop_intermediate.inheritance;

public class SubClass extends SuperClass {

	public String doSomething() {
		// return this.privAttr; // private, impossible
		System.out.println(this.getPrivAttr());
		return this.protAttr;

	}
}
