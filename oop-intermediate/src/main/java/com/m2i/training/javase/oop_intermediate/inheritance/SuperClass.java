package com.m2i.training.javase.oop_intermediate.inheritance;

public class SuperClass{
	public String pubAttr;
	protected String protAttr;
	private String privAttr = "hello";
	
	public String getPrivAttr() {
		return privAttr;
	}
	
}
