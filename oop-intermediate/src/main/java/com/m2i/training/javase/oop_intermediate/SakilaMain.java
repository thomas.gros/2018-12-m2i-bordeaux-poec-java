package com.m2i.training.javase.oop_intermediate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SakilaMain {

	public static void main(String[] args) {

// Avant Java 7

//		Connection con = null;
//
//		try {
//			con = 
//					DriverManager.getConnection(
//							"jdbc:mysql://127.0.0.1:3306/sakila?serverTimezone=UTC",
//							"root", "rootroot");
//			
//			Statement stmt = con.createStatement();
//			ResultSet rs = stmt.executeQuery("SELECT * FROM film");
//			while(rs.next())  {
//				System.out.println(rs.getString("title")); // rs.getString(2)
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if(con != null) {
//					con.close();
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}


		// try-with-resources
		// marche avec les classes qui implémentent
		// l'interface Closable
		try(Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sakila?serverTimezone=UTC","root", "rootroot")) {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM film");
			while(rs.next()) {
				System.out.println(rs.getString("title")); // rs.getString(2)
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

}
