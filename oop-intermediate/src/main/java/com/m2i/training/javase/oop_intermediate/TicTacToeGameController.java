package com.m2i.training.javase.oop_intermediate;

public class TicTacToeGameController {
	
	private Player player1;
	private Player player2;
	private Player currentPlayer;
	private Grid grid;

	public void run() {		
		initNewGame();
		playGame();
	}

	private void initNewGame() {
		player1 = new Player();
		player1.setSymbol('X');
	
		player2 = new Player();
		player2.setSymbol('O');
	
		currentPlayer = player1;
	
		grid = new Grid();
	}
	
	private void playGame() {
		GridOutput gridOutput = new GridOutput(grid);
		
		// JOUER LA PARTIE
		gridOutput.printGrid(); // tracer la grille

		// boucler tant qu'il y a des cases vides
		while (! grid.isFull()) {
			// joueur courant joue
			PlayerSymbolCoordinatesInput input = new PlayerSymbolCoordinatesInput();
			int[] coords = input.getPlayerSymbolCoordinates(currentPlayer);
			
			grid.put(currentPlayer, coords[0], coords[1]);

			// tracer la grille
			gridOutput.printGrid();

			// verifie si currentPlayer a gagné
			if (checkIfCurrentPlayerIsWinner()) {
				// si currentPlayer a gagné => arrêter le jeu + msg bravo
				System.out.println("Joueur " + currentPlayer.getSymbol() + " a gagné");
				break;
			}

			// currentPlayer = l'autre joueur
			switchPlayer();
		}

		// TODO cas du match nul ?	
	}

	private boolean checkIfCurrentPlayerIsWinner() {
		// TODO vérifier si il y a un vainqueur
		return false;
	}

	private void switchPlayer() {

		if (currentPlayer == player1) {
			currentPlayer = player2;
		} else {
			currentPlayer = player1;
		}

	}
	
}
