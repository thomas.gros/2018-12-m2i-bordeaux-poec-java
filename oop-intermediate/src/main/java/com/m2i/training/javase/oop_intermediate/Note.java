package com.m2i.training.javase.oop_intermediate;

// https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html
public class Note implements Comparable<Note> {

	private int value;

	public Note(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	// https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html
	@Override
	public int compareTo(Note o) {
		return value - o.value;
	}
	
}
