package com.m2i.training.javase.oop_intermediate;

import com.m2i.training.javase.oop_intermediate.inheritance.SubClass;
import com.m2i.training.javase.oop_intermediate.inheritance.SuperClass;

public class MainSuperSub {

	public static void main(String[] args) {
		SuperClass superc = new SuperClass();
		SubClass subc = new SubClass();
		
		System.out.println(superc);
		System.out.println(subc);
		
		System.out.println(superc.pubAttr);
		System.out.println(subc.pubAttr);
		
		System.out.println(subc.doSomething());
	}

}
