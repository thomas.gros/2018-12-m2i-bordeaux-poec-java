package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;

public class ShoppingCartv2 {

	private ArrayList<ShoppingCartItem> items;
	
	public ShoppingCartv2() {
		items = new ArrayList<ShoppingCartItem>();
	}

	public void add(Book book, int qty) {
		// qté < 0 ? book null ?, ...

		// ShoppingCartItem sci = new ShoppingCartItem(book, qty);
		// items.add(sci);
		
		items.add(new ShoppingCartItem(book, qty));
	}

	public ArrayList<ShoppingCartItem> getItems() {
		return items;
	}

}
