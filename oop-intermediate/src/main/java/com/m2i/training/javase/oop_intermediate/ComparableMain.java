package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ComparableMain {

	public static void main(String[] args) {

		Set<Note> notes = new TreeSet<Note>();
		
		notes.add(new Note(10));
		notes.add(new Note(8));
		notes.add(new Note(15));
		notes.add(new Note(4));
		notes.add(new Note(18));

		for (Note note : notes) {
			System.out.println(note.getValue());
		}
		
		List<Note> noteList = new ArrayList<Note>();
		noteList.add(new Note(10));
		noteList.add(new Note(8));
		noteList.add(new Note(15));
		noteList.add(new Note(4));
		noteList.add(new Note(18));
		
		Collections.sort(noteList);
		for (Note note : noteList) {
			System.out.println(note.getValue());
		}
	}

}
