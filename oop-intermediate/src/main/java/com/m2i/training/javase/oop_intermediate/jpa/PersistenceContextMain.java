package com.m2i.training.javase.oop_intermediate.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.m2i.training.javase.oop_intermediate.hibernate.Film;

public class PersistenceContextMain {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("pu1");
		
		EntityManager entityManager = 
				entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();

		// state transient
		Film f = new Film();
		f.setTitle("La POEC fait du ski");
		f.setDescription("lorem ipsum set dolor...");
		f.setReleaseYear("2019");
		f.setLanguageId(1);
		
		// state managed / persistent
		entityManager.persist(f);
		System.out.println(f);
		
		// detached (si pas detached ça ne fera pas de select)
		entityManager.detach(f);
		
		Film f2 = entityManager.find(Film.class, f.getId());
		System.out.println(f2);
		System.out.println(f == f2);
		
		entityManager.getTransaction().commit();

		entityManager.close();
		
		// après close() f est detached
		System.out.println(f);
		
		
		
		// ### find / getReference
		entityManager = 
				entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();

		// SELECT immédiat
//		f = entityManager.find(Film.class, new Long(1));
//		f.setTitle("un nouveau titre");
//		f.setReleaseYear("2006");
		
		System.out.println("avant chargement par référence");
		f = entityManager.getReference(Film.class, new Long(1));
		System.out.println("après chargement par référence");
		System.out.println("avant sysout f");
		System.out.println(f);
		System.out.println("après sysout f");
		f.setTitle("modif du titre par référence");
		f.setReleaseYear("2006");
		
		entityManager.getTransaction().commit();
		entityManager.close();
		
		
		entityManager = 
				entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();

		f = entityManager.find(Film.class, new Long(1));
		
		// entre temps la base de données a été modifiée
		
		entityManager.refresh(f); // rafraîchit par rapport à BDD
		
		
		entityManager.getTransaction().commit();
		entityManager.close();

		// FLUSH
		entityManager = 
				entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();

		f = entityManager.find(Film.class, new Long(1));
		f.setTitle("test de flush - 6");
		f.setReleaseYear("2006");
		f.setDescription("une description - 6");
		
		// SELECT * FROM film
		
		entityManager.getTransaction().commit();
		entityManager.close();
		
		
		// ### Fin du programme
		entityManagerFactory.close();
		
	}

}
