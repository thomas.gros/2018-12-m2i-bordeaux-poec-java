package com.m2i.training.javase.oop_intermediate;

public class StyloMain {

	public static void main(String[] args) {
		Stylo stylo = new Stylo();
		
		System.out.println(stylo.getBouchon());
		
		BouchonNormal bouchon1 = new BouchonNormal();
		GrandBouchon bouchon2 = new GrandBouchon();

		stylo.setBouchon(bouchon1);
		System.out.println(stylo.getBouchon().getColor());
		
		stylo.setBouchon(bouchon2);
		System.out.println(stylo.getBouchon().getColor());
	}

}
