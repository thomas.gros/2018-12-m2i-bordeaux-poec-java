package com.m2i.training.javase.oop_intermediate;

public class Player {
	private char symbol;
	
	public void setSymbol(char symbol) {
		this.symbol = symbol;
		
	}
	
	public char getSymbol() {
		return symbol;
	}

}
