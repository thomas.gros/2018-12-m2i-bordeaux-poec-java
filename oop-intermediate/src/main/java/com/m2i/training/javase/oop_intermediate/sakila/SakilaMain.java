package com.m2i.training.javase.oop_intermediate.sakila;

import java.util.List;

public class SakilaMain {

	public static void main(String[] args) {

		FilmRepository repo = new FilmRepository();	
		List<Film> films = repo.findAll();

		for (Film film : films) {
			System.out.println(film);
		}
		
		Film film = repo.find(1); // trouve le film d'id 1
								  // si n'existe pas retourne null 
		
		System.out.println(film);
		
		CustomerRepository customerRepo = new CustomerRepository();
		Customer customer = customerRepo.find(1);
		System.out.println(customer);
	}

}
