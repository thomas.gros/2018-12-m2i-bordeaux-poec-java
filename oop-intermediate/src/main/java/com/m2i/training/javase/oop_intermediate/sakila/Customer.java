package com.m2i.training.javase.oop_intermediate.sakila;

public class Customer {

	private String customerId;
	private String firstname;
	private String lastname;
	private Country country;

	public Customer(String customerId, String firstname, String lastname, Country country) {
		this.customerId = customerId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.country = country;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", country=" + country + "]";
	}
}
