package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;

public class EcommerceMainv2 {

	public static void main(String[] args) {

		Book book = new Book("Java c'est bien");
		Book book2 = new Book("Java 8 c'est bien");
		
		ShoppingCartv2 cart = new ShoppingCartv2();
		cart.add(book, 1);
		cart.add(book2, 5);
		
		ArrayList<ShoppingCartItem> items = cart.getItems();
		for(int i = 0; i < items.size(); i++) {
			if(items.get(i) != null) {
				System.out.println(items.get(i).getBook().getTitle() 
							 	   + " - " + items.get(i).getQty());
			}
		}

	}

}
