package com.m2i.training.javase.oop_intermediate;

public class Grid {

	private Player[][] data;
	
	public Grid() {
		data = new Player[3][3];
	}
	
	public boolean isFull() {
		for (int y = 0; y <= 2; y++) {
			for (int x = 0; x <= 2; x++) {
				if (data[x][y] == null) {
					return false;
				}
			}
		}

		return true;
	}

	public void put(Player player, int x, int y) {
		// TODO vérifier x, y
		data[x][y] = player;
	}

	public boolean isEmpty(int x, int y) {
		return data[x][y] == null;
	}

	public char getSymbolAt(int x, int y) {
		// TODO attention si data[x][y] == null
		return data[x][y].getSymbol();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}


