package com.m2i.training.javase.oop_intermediate;

public class Movie {

	private String title;
	private int rating;
	
	public Movie(String title, int rating) {
		super();
		this.title = title;
		this.rating = rating;
	}

	public String getTitle() {
		return title;
	}

	public int getRating() {
		return rating;
	}
	
}
