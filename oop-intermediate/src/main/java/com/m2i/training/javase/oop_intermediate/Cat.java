package com.m2i.training.javase.oop_intermediate;

public class Cat extends Animal {

	public Cat(String name) { // on hérite pas des constructeurs
		super(name); // appel du constructeur Animal(String)
	}
	
	@Override
	// annotation indiquant que la méthode 'override' 
	// une méthode de la classe mère ou d'une de ses super-classes
	public void makeSound() { // overrides
		System.out.println("Meow");
	}
	
}
