package com.m2i.training.javase.oop_intermediate;

import java.util.Scanner;

public class PlayerSymbolCoordinatesInput {

	private Scanner scanner;
	
	public PlayerSymbolCoordinatesInput() {
		scanner = new Scanner(System.in);
	}
	
	public int[] getPlayerSymbolCoordinates(Player player) {
		System.out.println("Joueur " + player.getSymbol() + " joue: ");
		String x = scanner.nextLine();
		String y = scanner.nextLine();
		
		int[] coordinates = {Integer.parseInt(x), Integer.parseInt(y)};
		return coordinates;
	}	
	
	
}
