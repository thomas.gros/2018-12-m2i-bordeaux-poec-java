package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class IterableMain {

	public static void main(String[] args) {
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("hello");
		strings.add("world");
		
		List<String> asList = strings;
		Collection<String> asACollection = strings;
		
		// https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html
		Iterable<String> asAnIterable = strings;
		
		// https://docs.oracle.com/javase/8/docs/api/java/util/Iterator.html
		Iterator<String> iterator = asAnIterable.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
			// iterator.remove();
		}
		
		
		// https://docs.oracle.com/javase/8/docs/technotes/guides/language/foreach.html
		for(String s: asAnIterable) { // for-each itère sur un Iterable
			System.out.println(s);
		}
	
		for(String s: strings) { // strings implémente Iterable
			System.out.println(s);
		}

	}

}
