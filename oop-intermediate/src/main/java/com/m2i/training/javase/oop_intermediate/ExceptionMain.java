package com.m2i.training.javase.oop_intermediate;

public class ExceptionMain {

	public static void main(String[] args) {
		System.out.println("Avant");
		
//		int[] arr = new int[2];
//		arr[2] = 42;
		
		try { 
			// on essaie d'exécuter 
			// du code qui potentiellement
			// jette une exception
			double rnd = Math.random();
			
			if(rnd < 0.3) {
				// RuntimeException ex = new RuntimeException("boom");
				// throw ex;
				throw new ArrayIndexOutOfBoundsException("boom");
			} else if (rnd < 0.6) {
				throw new RuntimeException("boom - runtime");
			}
			
		} catch (ArrayIndexOutOfBoundsException ex) {
			System.out.println(ex);
		} catch (RuntimeException ex) {
			System.out.println(ex);
		}
		
		System.out.println("Après");

	}

}
