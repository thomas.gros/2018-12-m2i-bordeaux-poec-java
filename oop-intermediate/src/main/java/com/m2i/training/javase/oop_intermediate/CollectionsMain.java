package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class CollectionsMain {

	public static void main(String[] args) {
//		ArrayList<String> strings = new ArrayList<String>();
//		LinkedList<String> strings = new LinkedList<String>();
		
		List<String> strings = new ArrayList<String>();
		
		strings.add("hello");
		strings.add("word");
		
		System.out.println(strings);

		
		// HashSet<String> setStrings = new HashSet<String>();
		// TreeSet<String> setStrings = new TreeSet<String>();
		// LinkedHashSet<String> setStrings = new LinkedHashSet<String>();
		
		Set<String> setStrings = new HashSet<String>();
		
		setStrings.add("hello");
		setStrings.add("world");
		System.out.println(setStrings);
	}

}
