package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;

public class ListMain {

	public static void main(String[] args) {

//		String[] tab = new String[10];
//		tab[0] = "hello";
//		tab[1] = "world";
		
		// Liste
		ArrayList<String> tabString = new ArrayList<String>();
		tabString.add("hello");
		tabString.add("world");
		System.out.println(tabString.size());
		System.out.println(tabString.isEmpty());
		System.out.println(tabString.contains("hello"));
		
//		for(int i = 0; i < tabString.size(); i++) {
//			System.out.println(tabString.get(i));
//		}
		
		for (String e : tabString) {
			System.out.println(e);
		}
		
	}

}
