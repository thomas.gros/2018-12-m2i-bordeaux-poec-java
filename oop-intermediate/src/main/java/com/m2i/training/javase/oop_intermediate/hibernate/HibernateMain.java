package com.m2i.training.javase.oop_intermediate.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateMain {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory = null;
		
// Obtenir une SessionFactory
		
		// A SessionFactory is set up once for an application!
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure() // configures settings from hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
		}
		catch (Exception e) {
			e.printStackTrace();
			// The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
			// so destroy it manually.
			StandardServiceRegistryBuilder.destroy( registry );
			System.exit(1);
		}

		// Obtenir une Session à partir d'une SessionFactory
		Session session = sessionFactory.openSession();
		// Démarrer une transaction
		session.beginTransaction();
		
		// effectuer notre requête
		Film f = session.find(Film.class, new Long(1));
		System.out.println(f);
		
		// Commiter une transaction
		session.getTransaction().commit();
		// Fermer la session
		session.close();
		
		sessionFactory.close();
		
	}

}
