package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;
import java.util.List;

public class MovieMain {

	public static void main(String[] args) {
		List<Movie> movies = new ArrayList<Movie>();
		
		movies.add(new Movie("E.T.", 8));
		movies.add(new Movie("Critters", 9));
		movies.add(new Movie("Vendredi 13", 7));
		
		// voir Collections.sort(list, comparator)
		movies.sort(new MovieTitleComparator());
		
		for (Movie movie : movies) {
			System.out.println(movie.getTitle());
		}
		System.out.println("=================");
		movies.sort(new MovieRatingComparator());
		for (Movie movie : movies) {
			System.out.println(movie.getTitle() 
								+ " - " + movie.getRating());
		}
		
	}

}
