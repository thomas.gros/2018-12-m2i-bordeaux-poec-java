package com.m2i.training.javase.oop_intermediate;

public interface Bouchon {

	static final int A_CONSTANT = 42;
	
	String getColor();
}
