package com.m2i.training.javase.oop_intermediate.sakila;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRepository {

	public Customer find(int customerId) {

		String sqlQuery = "SELECT customer.* , country.* " + 
						  "FROM customer " + 
						  	"INNER JOIN address " + 
						  		"ON customer.address_id = address.address_id " + 
						  	"INNER JOIN city " + 
						  		"ON address.city_id = city.city_id " + 
						  	"INNER JOIN country " + 
						  		"ON city.country_id = country.country_id " +
					  		"WHERE customer_id = ?";
		
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sakila?serverTimezone=UTC",
				"root", "rootroot")) {
			
			PreparedStatement stmt =  con.prepareStatement(sqlQuery);
			stmt.setInt(1, customerId);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				Country country = new Country(rs.getString("country_id"), rs.getString("country"));
				
				Customer customer = new Customer(rs.getString("customer_id"),
												 rs.getString("first_name"),
												 rs.getString("last_name"),
												 country);
				return customer;
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			// en pratique on va plutôt créer une RepositoryException
			throw new RuntimeException(e);
		}
	}

}
