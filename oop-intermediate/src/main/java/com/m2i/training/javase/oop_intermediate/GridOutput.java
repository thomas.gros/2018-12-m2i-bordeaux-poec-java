package com.m2i.training.javase.oop_intermediate;

public class GridOutput {

	private Grid grid;
	
	public GridOutput(Grid grid) {
		this.grid = grid;
	}
	
	public void printGrid() {
		for (int j = 0; j <= 2; j++) {
			System.out.println("-------------");
			System.out.print("| ");
			for (int i = 0; i <= 2; i++) {
				if(grid.isEmpty(i, j)) {
					System.out.print(" ");
				} else {
					System.out.print(grid.getSymbolAt(i, j));
				}
				System.out.print(" | ");
			}
			System.out.println("");
		}
		System.out.println("-------------");
	}

	

}
