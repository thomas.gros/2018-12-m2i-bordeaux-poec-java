package com.m2i.training.javase.oop_intermediate;

import java.util.ArrayList;

public class Playlist {

	private ArrayList<Song> songs;
	
	public Playlist() {
		songs = new ArrayList<Song>();
	}
	
	public void addSong(Song song) {
		if(song == null) {
			throw new NullPointerException("Song must not be null");
		}
		songs.add(song);
	}

}