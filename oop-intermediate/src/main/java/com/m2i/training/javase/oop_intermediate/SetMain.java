package com.m2i.training.javase.oop_intermediate;

import java.util.HashSet;

public class SetMain {

	public static void main(String[] args) {
		
		HashSet<String> set = new HashSet<String>();
		
		set.add("hello");
		System.out.println(set.size());
		
		set.add("world");
		System.out.println(set.size());
		
		set.add("world");
		System.out.println(set.size());
		
		System.out.println(set.contains("world"));
		System.out.println(set.contains("hello"));
		System.out.println(set.contains("test"));
		
		for (String e : set) {
			System.out.println(e);
		}


	}

}
