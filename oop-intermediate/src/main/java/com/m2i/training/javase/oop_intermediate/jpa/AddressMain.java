package com.m2i.training.javase.oop_intermediate.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.m2i.training.javase.oop_intermediate.hibernate.Address;
import com.m2i.training.javase.oop_intermediate.hibernate.Customer;

public class AddressMain {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("pu1");
		
		EntityManager entityManager = 
				entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();

		Address a = entityManager.find(Address.class, new Long(1));
		System.out.println(a);

		entityManager.getTransaction().commit();

		entityManager.close();
		
		entityManagerFactory.close();

	}

}
