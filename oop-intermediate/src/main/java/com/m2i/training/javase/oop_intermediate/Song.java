package com.m2i.training.javase.oop_intermediate;

public class Song {

	private String title;
	private int duration;
	
	/**
	 * Constructs a new Song.
	 * 
	 * @param title the title of the song
	 * @param duration the duration in seconds
	 * @throws NullPointerException if title is null
	 * @throws IllegalArgumentException if title is empty
	 * @throws IllegalArgumentException if duration is negative or null
	 */
	public Song(String title, int duration) {
		if(title == null) {
			throw new RuntimeException(
					"Title must not be null");
		}
		
		if(title.isEmpty()) {
			throw new IllegalArgumentException(
					"Title must not be empty");
		}
		
		if(duration <= 0) {
			throw new IllegalArgumentException(
					"Duration must be strictly positive");
		}

		this.title = title;
		this.duration = duration;
	}
	
	
	
}
