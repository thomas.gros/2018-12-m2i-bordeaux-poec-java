console.log('coucou');

let http = require('http');

const options = {
    host: "localhost",
    port: 8080,
    path: "/servlet-0.0.1-SNAPSHOT/api/hello"
    //, method: "GET"
}

function handler(response) {
    console.log(response.statusCode);
    console.log(response.rawHeaders);

    let data = '';
    response.on('data', function(chunk) {
        data += chunk;
    });

    response.on('end', function() {
        console.log('received: ' + data);
    });

}

let request = http.request(options, handler);
request.end();

