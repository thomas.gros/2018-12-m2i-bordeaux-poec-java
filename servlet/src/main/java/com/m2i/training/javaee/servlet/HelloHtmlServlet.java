package com.m2i.training.javaee.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloHtmlServlet
 */
@WebServlet("/hellohtml")
public class HelloHtmlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, 
						 HttpServletResponse response) throws ServletException, IOException {
		
		response.setStatus(200); 
		
		response.setHeader("Content-Type", "text/html");
		
		response
			.getWriter()
			.append("<!doctype html>")
			.append("<html>")
			.append("<body>")
			.append("<p>hello :-)</p>")
			.append("</body>")
			.append("</html>");
		
//		PrintWriter out = response.getWriter();
//		out.append("<!doctype html>");
//		out.append("<html>");
		
//		PrintWriter out = response.getWriter();
//		out.append("<!doctype html>")
//		   .append("<html>");
	}

}
