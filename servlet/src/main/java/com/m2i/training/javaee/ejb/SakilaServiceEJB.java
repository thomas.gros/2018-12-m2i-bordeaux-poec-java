package com.m2i.training.javaee.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.m2i.training.javaee.jpa.Film;

@Stateless
public class SakilaServiceEJB {

	@PersistenceContext(unitName="pu1")
	private EntityManager entityManager;
	
	// Les méthodes d'un EJB stateless sont 
	// appelées "dans une transaction"
	// et dans un "try catch"
	public Film find(Long id) {
		return entityManager.find(Film.class, id);
	}

	public void addFilm(String title) {
		Film f = new Film();
		f.setTitle(title);
		f.setLanguageId(1);
		
		entityManager.persist(f);
	}
}