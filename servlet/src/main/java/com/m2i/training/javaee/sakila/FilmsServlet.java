package com.m2i.training.javaee.sakila;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FilmsServlet
 */
@WebServlet("/films")
public class FilmsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// declarer le connecteur mysql dans 
		// le pom.xml
		
		FilmRepository filmRepo = new FilmRepository();
		
		List<Film> films = filmRepo.findAll(); // JDBC
		
		request.setAttribute("films", films);
		
		request.getRequestDispatcher("/WEB-INF/films.jsp")
			   .forward(request, response);
		
//		response.setHeader("Content-Type", "text/html");
//		
//		PrintWriter out = response.getWriter();
//		out.append("<!doctype html>")
//			.append("<html>")
//			.append("<body>");
//		
//		out.append("<table>");
//		
//		for(Film f: films) {
//			String filmURL = "http://localhost:8080/servlet-0.0.1-SNAPSHOT/film/" + f.getFilmId();
//			response.getWriter()
//					.append("<tr>")
//					.append("<td>"+ f.getFilmId() + "</td>")
//					.append("<td><a href=\""+ filmURL +"\">"+ f.getTitle() + "</a></td>")
//					.append("</tr>");
//		}
//		
//		out.append("</table>");
//		out.append("</body>")
//		   .append("</html>");		
	}


}
