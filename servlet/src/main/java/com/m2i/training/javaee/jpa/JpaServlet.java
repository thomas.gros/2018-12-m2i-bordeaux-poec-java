package com.m2i.training.javaee.jpa;

import java.io.IOException;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Servlet implementation class JpaServlet
 */
@WebServlet("/jpa")
public class JpaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@PersistenceUnit(unitName="pu1")
	private EntityManagerFactory entityManagerFactory;
	
	@Resource
	private UserTransaction utx;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		EntityManager entityManager = 
				entityManagerFactory.createEntityManager();
		
		// entityManager.getTransaction().begin();
		try {
			utx.begin();
			
			Film f = entityManager.find(Film.class, new Long(1));
			System.out.println(f);

			response.getWriter()
					.append(f.toString());
			
			utx.commit();
			
			// entityManager.getTransaction().commit();
		} catch (NotSupportedException | SystemException | SecurityException | IllegalStateException | RollbackException | HeuristicMixedException | HeuristicRollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		


		entityManager.close();
	}

}
