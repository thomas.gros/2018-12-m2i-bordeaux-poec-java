package com.m2i.training.javaee.ejb;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/jpaejb-add-film")
public class JpaEjbAddFilmServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SakilaServiceEJB sakilaService;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/add-film.jsp")
			   .forward(request, response);
	}

	private Map<String, String> validateForm(HttpServletRequest request) {
		
		Map<String, String> errors 
			= new HashMap<String, String>();

		// title
		String title = request.getParameter("title");
		
		if(title == null || title.isEmpty()) {
			errors.put("title", "le titre ne doit pas être vide");
		} else if (title.length() < 2) {
			errors.put("title", "la longueur du titre doit être > 2");
		}
		
		// description
		String description = request.getParameter("description");
		
		if(description == null || description.isEmpty()) {
			errors.put("description", "la description ne doit pas être vide");
		}
		
		return errors;
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// vérifier que les données envoyées sont valides
		Map<String, String> formErrors = validateForm(request);
		if(! formErrors.isEmpty()) {
			request.setAttribute("formErrors", formErrors);
			request.getRequestDispatcher("/WEB-INF/add-film.jsp")
			   	   .forward(request, response);
			return;
		}
		
		// Persister le film
		String title = request.getParameter("title");
		// Si exception, le serveur Java EE retourne une 500
		sakilaService.addFilm(title); 
		
		// rediriger vers une autre page
		response.sendRedirect("films");
	}

}
