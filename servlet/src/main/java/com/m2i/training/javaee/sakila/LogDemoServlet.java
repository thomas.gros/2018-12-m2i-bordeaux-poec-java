package com.m2i.training.javaee.sakila;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;


@WebServlet("/log")
public class LogDemoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// logger de java.util.logging
	private static final Logger LOGGER 
			= Logger.getLogger(LogDemoServlet.class.getName());
	
	// logger de org.slf4j
	private static final org.slf4j.Logger LOGGER_SLF4J
			= LoggerFactory.getLogger(LogDemoServlet.class.getName());

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LOGGER.fine("ceci est un log niveau fine");
		LOGGER.finer("ceci est un log niveau finer");
		LOGGER.finest("ceci est un log niveau finest");
		LOGGER.info("ceci est un log niveau info");
		LOGGER.warning("ceci est un log niveau warning");
		LOGGER.severe("ceci est un log niveau severe");
		LOGGER.log(Level.INFO, "ceci est un log niveau info");
		
		LOGGER_SLF4J.trace("ceci est un log niveau trace");
		LOGGER_SLF4J.debug("ceci est un log niveau debug");
		LOGGER_SLF4J.info("ceci est un log niveau info");
		LOGGER_SLF4J.warn("ceci est un log niveau warning");
		LOGGER_SLF4J.error("ceci est un log niveau error");
		
		response.getWriter().append("Served at: ").append(request.getContextPath());

	}

}
