package com.m2i.training.javaee.sakila;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FilmServlet
 */
@WebServlet("/film/*")
public class FilmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, 
						 HttpServletResponse response) throws ServletException, IOException {

		String pathInfo = request.getPathInfo();
		
		if(pathInfo == null) {
			response.setStatus(404);
			return; // ne pas continuer après le if !!!
		}
		
		// le premier slash a la position 0
		pathInfo = pathInfo.substring(1);
		
//		for(int i = 0; i < pathInfo.length(); i++) {
//			if(! Character.isDigit(pathInfo.charAt(i))) {
//				response.setStatus(404);
//				return;
//			}
//		}

		int filmId;
		
		try {
			filmId = Integer.parseInt(pathInfo);
		} catch (NumberFormatException e) {
			response.setStatus(404);
			return;
		}
		
		FilmRepository filmRepo = new FilmRepository();
		
		Film film = filmRepo.find(filmId);
		
		if(film == null) {
			response.setStatus(404);
			return;
		}
		
		// response.setHeader("Content-Type", "text/html");
		
		// PrintWriter out = response.getWriter();
		// out.append("<h1>" + film.getTitle() + "</h1>");
		// out.append("<p>" + film.getDescription() + "</p>");
		
		request.setAttribute("film", film);
		
		request.getRequestDispatcher("/WEB-INF/film.jsp")
			   .forward(request, response);
		
	}

}
