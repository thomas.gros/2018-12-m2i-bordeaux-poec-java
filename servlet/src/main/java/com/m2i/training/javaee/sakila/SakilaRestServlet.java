package com.m2i.training.javaee.sakila;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/api/films/*")
public class SakilaRestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fromRaw = request.getParameter("from");
		String countRaw = request.getParameter("count");
	
		// TODO valider les paramètres et gérer les erreurs
		int from = Integer.parseInt(fromRaw);
		int count = Integer.parseInt(countRaw);
		
		FilmRepository filmRepo = new FilmRepository();
		List<Film> films = filmRepo.findAll(from, count);
		
		response.setCharacterEncoding("UTF-8"); // UTF-8 pour JSON
		response.setHeader("Content-Type", "application/json");

		// http://enable-cors.org
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		// TODO films => JSON
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		response.getWriter().append(filmsToJSON(films));
	}
	
	private String filmsToJSON(List<Film> films) {
		StringBuilder sb = new StringBuilder();
		// StringBuilder pas thread-safe
		// StringBuffer synchronized / thread-safe
		sb.append("[");
		for(int i = 0; i < films.size(); i++) {
			sb.append(filmToJSON(films.get(i)));
			if(i != films.size() - 1) {
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	private String filmToJSON(Film f) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
			sb.append("\"film_id\":");
			sb.append(f.getFilmId());
			sb.append(",");
			sb.append("\"title\":");
			sb.append("\""+f.getTitle()+"\"");
		sb.append("}");
		return sb.toString();
	}

	
	
}
