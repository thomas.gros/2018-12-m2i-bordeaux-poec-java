package com.m2i.training.javaee.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class About
 */
@WebServlet("/about")
public class AboutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Content-Type", "text/html");
		
		response
			.getWriter()
			.append("<!doctype html>")
			.append("<html>")
			.append("<body>")
			.append("<h1>About</h1>")
			.append("<a href=\"http://localhost:8080/servlet-0.0.1-SNAPSHOT/home\">home</a>")
			.append("<a href=\"http://localhost:8080/servlet-0.0.1-SNAPSHOT/about\">about absolu</a>")
			.append("<a href=\"about\">about relatif</a>")
			.append("</body>")
			.append("</html>");
	}

}
