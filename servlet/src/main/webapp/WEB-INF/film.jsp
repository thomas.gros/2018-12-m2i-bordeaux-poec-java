<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- déconseillé, ne pas être tenté d'appeler 
des méthodes dans la JSP -->
${ film.getTitle() }

<!-- conseillé, manière déguisée d'appeler le getter -->  
<h1>${ film.title }</h1>

${ film.getDescription() } <!-- déconseillé -->
<p>${ film.description }</p>
</body>
</html>