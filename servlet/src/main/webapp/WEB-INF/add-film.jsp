<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" method="POST">
		<label for="title">title</label>
		<input type="text" 
			   id="title" 
			   name="title"
			   value="${ param.title }">
		<span>${ formErrors.title }</span>

		<label for="description">description</label>
		<textarea id="description" 
				  name="description">${ param.description }</textarea>
		<span>${ formErrors.description }</span>
		
		<input type="submit" value="Create new film">
	</form>
</body>
</html>