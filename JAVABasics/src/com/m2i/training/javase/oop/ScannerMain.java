package com.m2i.training.javase.oop;

import java.util.Scanner;

public class ScannerMain {

	public static void main(String[] args) {
		System.out.println("hello world");
		
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			String input = sc.nextLine();
			System.out.println("vous avez tappé: " + input);
			if(input.equals("quit")) {
				break;
			}
		}
		

	}

}
