package com.m2i.training.javase.oop;

public class DogMain {

	public static void main(String[] args) {
		System.out.println("Dans main de DogMain");
		
		Dog d1; // déclaration d'une variable d1 de type Dog
		
		// création d'un nouvel objet Dog
		// et affectation de cet objet à la variable d1
		d1 = new Dog();
		
		Dog d2 = new Dog();
		Dog d3 = new Dog();
		
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
		
		Dog d4 = d3;
		System.out.println(d4);
		d4 = d2;
		System.out.println(d3);
		System.out.println(d4);
		
		d1.name = "Medor";
		d2.name = "Fido";
		d3.name = "Rantanplan";
		
		System.out.println(d1.name);
		System.out.println(d2.name);
		System.out.println(d3.name);
		
		d1.color = "brown";
		d2.color = "FA5B";
		d3.color = "blue";
		
		d1.isHungry = true;
		d2.isHungry = true;
		d3.isHungry = false;
		
		System.out.println(d1.name);
		System.out.println(d1.color);
		System.out.println(d1.isHungry);
		
	}

}
