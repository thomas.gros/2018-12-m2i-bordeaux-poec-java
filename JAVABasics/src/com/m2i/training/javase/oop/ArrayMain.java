package com.m2i.training.javase.oop;

public class ArrayMain {

	public static void main(String[] args) {
	
		int[] ints = {1,2,3,4,5};
				
		int[] tabEntiers = new int[10];
		String[] tabString = new String[4];
		Bicyclette[] tabBicy = new Bicyclette[3];
		Car[] tabCar = new Car[50];
		
		System.out.println(tabEntiers[0]);
		System.out.println(tabEntiers[1]);
		
		System.out.println(tabEntiers.length);
		
		tabEntiers[4] = 42;
		
		for (int i = 0; i < tabEntiers.length; i++) {
			System.out.println(tabEntiers[i]);
		}
		
		for (int i = 0; i < tabString.length; i++) {
			System.out.println(tabString[i]);
		}
		
		// multi-dimensionnels
		
		int[] tab1Dim = new int[4];
		// [0,0,0,0]
		
		int[][] tab2Dim = new int[4][2];

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
