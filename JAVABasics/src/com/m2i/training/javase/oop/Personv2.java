package com.m2i.training.javase.oop;

public class Personv2 {

	// variables d'instance
	private String firstname;
	private String lastname;
	
	public Personv2(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
		System.out.println("dans le constructeur, hahaha");
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
