package com.m2i.training.javase.oop;

public class Person {
	
	// 1) variables d'instance
	private String firstname;
	private String lastname;
	
	// 2) getters / setters
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) { 
		this.lastname = lastname;
	}

	// 3) autres méthodes d'instance
	
	// écrire la méthode getFullname qui retourne la concaténation
	// du nom et du prénom
	
	public String getFullname() {
		return firstname + " " + lastname;
	}
	/* 
	 * écrire une méthode getFullnameWithPrefix qui 
	 * prend en paramètre un prefix de type String (par ex: Mr., Ms.)
	 * retourne le full name préfixé du préfixe
	 */	
	public String getFullnameWithPrefix(String prefix) {
		return prefix + " " + firstname + " " + lastname;
	}	
	
}
