package com.m2i.training.javase.oop;

/**
 * Voir https://docs.oracle.com/javase/tutorial/java/data/numberclasses.html
 * @author thomasgros
 *
 */
public class NumberMain {

	public static void main(String[] args) {
		// byte, short, int, long, float, double, boolean, char
		
		// Types Wrapper:
		// Byte, Short, Integer, Long, Float, Double, Boolean, Char
		
		int i1 = 1;
		Integer i2 = new Integer(18);
		
		int i3 = new Integer(1); // unboxing
				// (new Integer(1)).intValue();
		
		Integer i4 = 1; // boxing
				// new Integer(1);
		
		// méthodes statiques
		System.out.println(Integer.MIN_VALUE);
		System.out.println(Integer.MAX_VALUE);
		
		System.out.println(Integer.toHexString(123123));
		System.out.println(Integer.compare(4, 10));
		
		// méthodes d'instance
		Integer i5 =  new Integer(4);
		Integer i6 =  new Integer(10);
		System.out.println(i5.compareTo(i6)); 
		
		
		
		
		
		
		
		

	}

}
