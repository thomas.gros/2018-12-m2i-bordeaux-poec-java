package com.m2i.training.javase.oop;

public class NotesMain {

	public static void main(String[] args) {
		
		int[] notes = {12, 16, 7, 18, 4, 9, 11};
		
		// 1) Calculer la somme des notes
		int sum = 0;
		for (int i = 0; i < notes.length; i++) {
			sum += notes[i]; 
		}
		
		// 2) Calculer la moyenne des notes
		double avg = (1.0 * sum) / notes.length;
		
		// 3) Trouver la note max
		int max = notes[0];
		for (int i = 1; i < notes.length; i++) {
			if(notes[i] > max) {
				max = notes[i];
			}
		}
		
		// 4) Trouver la note min
		int min = notes[0];
		for (int i = 1; i < notes.length; i++) {
			if(notes[i] < min) {
				min = notes[i];
			}
		}
		
		
		
		
		
		
		
		
		
		
	}

}
