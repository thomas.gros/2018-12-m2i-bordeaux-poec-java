package com.m2i.training.javase.oop;

/**
 * Cf:
 * https://docs.oracle.com/javase/tutorial/java/data/strings.html
 * 
 * A lire:
 * http://kunststube.net/encoding/
 * @author thomasgros
 *
 */
public class StringMain {
	
	public static void main(String[] args) {

		String s1 = "Hello Java";
		String s2 = new String("Hello Java");
		
		System.out.println(s1);
		System.out.println(s2);
		
		int length = s1.length();
		System.out.println(length);
		
		char c0 = s1.charAt(0);
		System.out.println(c0);
		System.out.println(s1.charAt(1));
		
		// exercice 1)
		// écrire un programme qui affiche
		// chaque caractère de s1, un caractère par ligne
		for (int i = 0; i < s1.length(); i++) {
			System.out.println(s1.charAt(i));
		}
		
		int cpt = 0;
		while (cpt < s1.length()) {
			System.out.println(s1.charAt(cpt));
			cpt++;
		}
		
		// exercice 2)
		// cf exercice 1 mais afficher les caractères dans l'ordre
		// inverse
		for (int i = s1.length() - 1; i >=0 ; i--) {
			System.out.println(s1.charAt(i));
		}
		

		// exercice 3)
		// cf exercice 2 mais n'afficher que les voyelles
		for (int i = s1.length() - 1; i >=0 ; i--) {
			char currentChar = s1.charAt(i);
			if(currentChar == 'a'
			|| currentChar == 'e'
			|| currentChar == 'i'
			|| currentChar == 'o'
			|| currentChar == 'u'
			|| currentChar == 'y') {
				System.out.println(currentChar);
			}
		}
		
		// exercice 4)
		// String mot = "...."
		// Afficher "est un palindrome" si mot est un palindrome
		// Afficher "n'est pas un palindrome" si un mot 
		// n'est pas un palindrome
		
		// écrire du pseudo-code puis le traduire en code
		// mot = "radar"
		String mot = "radar";

		// median = arrondi_supérieur(longueur mot / 2)
		// int median = (int) Math.ceil(mot.length() / 2);
		double median = mot.length() / 2;
			
		// leMotEstUnPalindrome = true
		boolean isPalindrom = true;
		
		// boucler i de 0 à médian
		for(int i = 0; i < median; i++) {
			// si mot[i] != mot[mot.longueur - 1 - i]
			if(mot.charAt(i) != mot.charAt(mot.length() - 1 - i)) {
				// leMotEstUnPalindrome = false
				isPalindrom = false;
				// break;
				break;
			}
		}
		// sysout(leMotEstUnPalindrome)
		System.out.println(isPalindrom);
		
		
		
		
		
		
		
		
		
		
		
		
		
		String s3 = "pas vide";
		String s4 = "";
		System.out.println("s3 est vide ? " + s3.isEmpty());
		System.out.println("s4 est vide ? " + s4.isEmpty());
		
		System.out.println(s3.substring(3));
		System.out.println(s3.substring(2, 5));
		System.out.println("chaine 1" + "chaine 2");
		System.out.println(s1.concat(s2));	
	}

}

