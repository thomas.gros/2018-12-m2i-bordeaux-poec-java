package com.m2i.training.javase.oop;

public class CalculMain {

	public static void main(String[] args) {
		
		String data = "hello world";
		
		Calcul calc = new Calcul();
		int vowelsCount = calc.countVowels(data); // 3
		
		// pourcentage de voyelles par rapport aux lettres
		// du paramètre.
		// les espaces ne comptent pas comme des lettres
		// Le pourcentage doit être exprimé entre 0 et 1
		double percentOfVowels = calc.percentOfVowels(data); // 0.3
		System.out.println(percentOfVowels);
		
		
//		calc.countVowels("hello");
//		calc.countVowels("world");
//		calc.countVowels("vive");
//		calc.countVowels("java");
		
		// getTotalVowelsCount donne le nombre
		// total de voyelles comptées par la méthode countVowels
//		double total = calc.getTotalVowelsCount();
		
		// getAverageVowelsCount donne la moyenne du
		// nombre de voyelles comptées  par la méthode countVowels
//		double avg = calc.getAverageVowelsCount();

		
				
		
		
		

	}

}
