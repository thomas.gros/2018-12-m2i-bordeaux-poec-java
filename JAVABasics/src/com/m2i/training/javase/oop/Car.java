package com.m2i.training.javase.oop;

public class Car {
	
	// variable d'instance, initialisée 
	// avec une valeur par défaut
	int speed;
	
	boolean speedLimiterOn;

	public void accelerate() {
		// System.out.println("j'accélère !!!");
		// System.out.println("vroom !!!");
		
		int speedLimit = 20;
		int accelerationRequired = 10;
		
		if(speedLimiterOn) {
			if(speed < speedLimit) {
				if(speedLimit - speed >= accelerationRequired) {
					speed += accelerationRequired;
				} else {
					// speed += speedLimit - speed;
					speed = speedLimit;
				}
			}
		} else {
			speed += accelerationRequired; // speed = speed + 10;
		}	
	}

	public void brake(int amount) {
		System.out.println("Je freine !!!");
		
		if(speed > amount) {
			speed -= amount;	
		} else {
			speed = 0;
		}
		
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
