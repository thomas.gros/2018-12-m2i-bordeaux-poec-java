package com.m2i.training.javase.oop;

public class StringUtil {

	public static int countVowels(String data) {
		int count = 0;
		
		for(int i = 0; i < data.length(); i++) {
			char c = data.charAt(i);
			
			if(c == 'a' || c == 'e' || c == 'i'
			|| c == 'o' || c == 'u' || c == 'y') {
				count++;
			}
		}
		
		return count;
	}
	
}
