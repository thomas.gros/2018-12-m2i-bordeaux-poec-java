package com.m2i.training.javase.oop;

public class Calcul {

	private boolean isVowel(char c) {
//		if(c == 'a'
//		|| c == 'e'
//		|| c == 'i'
//		|| c == 'o'
//		|| c == 'u'
//		|| c == 'y') {
//			return true;
//		} else {
//			return false;
//		}
		
		return c == 'a' || c == 'e' || c == 'i'
			|| c == 'o' || c == 'u' || c == 'y';
	}
	
	public int countVowels(String data) {
		
		int count = 0;
		
		for(int i = 0; i < data.length(); i++) {
			char currentChar = data.charAt(i);
			
			if(isVowel(currentChar)) {
				count++;
			}
		}
		
		return count;
	}
	
	public double percentOfVowels(String data) {
		int countVowels = countVowels(data);
		
		int totalCharWithoutSpace = 0;
		
		for(int i = 0; i < data.length(); i++) {
			if(data.charAt(i) != ' ') {
				totalCharWithoutSpace++;
			}
		}
		
		System.out.println(countVowels);
		System.out.println(totalCharWithoutSpace);
		//0.3 => 0
		
		return (1.0 * countVowels) / totalCharWithoutSpace;
		// cast - transtypage
		// return ((double) countVowels) / totalCharWithoutSpace;		
	}

}


















