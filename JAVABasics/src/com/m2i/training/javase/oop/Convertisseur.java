package com.m2i.training.javase.oop;

public class Convertisseur {

	public double convert(double value, String fromUnit, String toUnit) {

		if(fromUnit == toUnit) {
			return value;
		}
		
		// si fromUnit est en cm ET toUnit est en in
		if (fromUnit == "cm" && toUnit == "in") {
			// valeurEnInch = value / 2.54
			double valeurEnInch = value / 2.54;
			// retourne valeurEnInch
			return valeurEnInch;
		}

		// si fromUnit est en Inch ET toUnit est en cm
		if (fromUnit == "in" && toUnit == "cm") {
			// valeurEnCm = valeurEnInch * 2.54
			double valeurEnCm = value * 2.54;
			// retourne valeurEnCm
			return valeurEnCm;
		}

		return -1; // en attente de savoir gérer les erreurs (Exceptions)

	}

	public double convert2(double value, String fromUnit, String toUnit) {
		double result;
		
		if(fromUnit == toUnit) {
			result = value;
		} else if (fromUnit == "cm" && toUnit == "in") {
			result = value / 2.54;
		} else if (fromUnit == "in" && toUnit == "cm") {
			result = value * 2.54;
		} else {
			result =  -1;
		}

		return result;

	}

}
