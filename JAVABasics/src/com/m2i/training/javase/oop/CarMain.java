package com.m2i.training.javase.oop;

public class CarMain {

	public static void main(String[] args) {
		// int uneAutreVariable; // locale, non initialisée
		// System.out.println(uneAutreVariable);
		
		Car car1 = new Car();
		System.out.println(car1.speed); // 0
		car1.accelerate();
		System.out.println(car1.speed);
		car1.accelerate();
		System.out.println(car1.speed);
		//car1.brake();
		
		car1.speedLimiterOn = true;
		// MAX = 20
		
		car1.speedLimiterOn = true;
		car1.speed = 19;
			
		car1.accelerate();
		car1.accelerate();
		car1.accelerate();
		car1.accelerate();

		car1.speed = 10;
		System.out.println(car1.speed);
		car1.brake(5);
		System.out.println(car1.speed);
		car1.brake(7);
		System.out.println(car1.speed);		
		
		
		
	}

}
