package com.m2i.training.javase.oop;

import java.util.Random;
import java.util.Scanner;

public class GuessANumber {

	public static void main(String[] args) {
		Random random = new Random();
		int numberToGuess = 1 + random.nextInt(10); //[1..10]
		
		boolean hasWon = false;
		
		Scanner scanner = new Scanner(System.in);
		
		while (hasWon == false) {
			System.out.println("Deviner le nombre");
			String answer = scanner.nextLine();
			
			if(Integer.parseInt(answer) == numberToGuess) {
				System.out.println("Gagné");
				hasWon = true;
			}
		}

	}

}