package com.m2i.training.javase.basics;

/**
 * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
 * @author thomasgros
 *
 */
public class OperatorMain {

	public static void main(String[] args) {
		
		// précédence https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
		int i1 = 32;
		int i2 = 31 + 1;
		int result = 32 * ((1 - 2) / 4);

		
		int i3 = 3;
		i3 += 10;      // i3 = i3 + 10;
		System.out.println(i3);
		
		int i4 = i3 += 5;
		System.out.println(i3);
		System.out.println(i4);
		
		// assignement, arithmetics, unary
		// https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op1.html
		System.out.println(1 / 2); // division entière
		System.out.println(1.0 / 2); // flottant
		System.out.println(1 / 2.0);
		System.out.println(1f / 2);
		System.out.println(1 / 2d);
		System.out.println(1.0 / 2d);
		
		
		int i5 = 2 + 3;
		double i6 = 2.0 + 3;
		String concat = "bonjour" + 3; // concatenation
		System.out.println(concat);
		String concat2 = "bonjour" + 2.0;
		System.out.println(concat2);
		
		// String s = "bonjour" + 2 + 3 / 3 % 24;
		// System.out.println(s);
		
		System.out.println("bonjour" + " " + "java");
		System.out.println("i6 = " + i6);
		
		// modulo % 
		
		// a % b  == reste de la division entière de a par b
		
		// a = b * q + r
		// 4 = 2 * q + r
		// 4 = 2 * 2 + 0 <= modulo
		
		// 5 = 2 * q + r
		// 5 = 2 * 2 + 1 <= modulo

		// 13 % 2
		// 13 = 2 * 6 + 1

		// 15 % 3
		// 15 = 3 * 5 + 0
		
		// 17 % 6
		// 17 = 6 * 2 + 5
		
		
		int m1 = 4 % 2;
		System.out.println("m1 = " + m1);
		
		int m2 = 5 % 2;
		System.out.println("m2 = " + m2);		
		
		
		// unaires
		int i7 = -5;
		System.out.println("i7=" + i7);
		
		int i8 = i7++;
		System.out.println("i7=" + i7);
		System.out.println("i8=" + i8);
		int i9 = ++i7;
		System.out.println("i7=" + i7);
		System.out.println("i9=" + i9);
		
		int i10 = i7--;
		System.out.println("i7=" + i7);
		System.out.println("i10=" + i10);
		
		int i11 = --i7;
		System.out.println("i7=" + i7);
		System.out.println("i11=" + i11);		
		
		
		// https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op2.html
		// Equality, Relational, and Conditional Operators
		
		boolean res1 = 5 == 5;
		System.out.println("5 == 5 " + res1);
		
		boolean res2 = 5 == 4;
		System.out.println("5 == 4 " + res2);
		
		int i12 = 42;
		
		boolean res3 = i12 == i6;
		System.out.println("i12 == i6 " + res3);
		
		boolean res4 = i12 != i6;
		System.out.println("i12 != i6 " + res4);
		
		boolean res5 = i12 > i6;
		boolean res6 = i12 >= i6;
		boolean res7 = i12 < i6;
		boolean res8 = i12 <= i6;
		
		
		// conditional operators
		// bool op bool => bool
		
		// && et || sont les shortcuts de & et |
		System.out.println("true &&(AND) true = " + (true && true));
		System.out.println("true &&(AND) false = " + (true && false));
		System.out.println("false &&(AND) true = " + (false && true));
		System.out.println("false &&(AND) false = " + (false && false));
		
		System.out.println("true ||(OR) true = " + (true || true));
		System.out.println("true ||(OR) false = " + (true || false));
		System.out.println("false ||(OR) true = " + (false || true));
		System.out.println("false ||(OR) false = " + (false || false));
		
		System.out.println("true &(AND) true = " + (true & true));
		System.out.println("true |(OR) true = " + (true | true));
		
		
		System.out.println("true ^(XOR) true = " + (true ^ true));
		System.out.println("true ^(XOR) false = " + (true ^ false));
		System.out.println("false ^(XOR) true = " + (false ^ true));
		System.out.println("false ^(XOR) false = " + (false ^ false));
		
		// ^ est équivalent à != pour si opérandes boolean.
		
		
		boolean b1 = (true || false) && (false || true);
		
		boolean b2 = (i4 == i3) || (i5 == i8);
		
	}
	
	
	
	
	
	
	
	
	
	
	

}
