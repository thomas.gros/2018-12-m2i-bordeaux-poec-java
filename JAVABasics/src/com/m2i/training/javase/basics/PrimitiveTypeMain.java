package com.m2i.training.javase.basics;

/**
 * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html
 * @author thomasgros
 */
public class PrimitiveTypeMain {

	public static void main(String[] args) {
		// 8 types primitifs
		
		// boolean: true / false
		boolean b1 = true;
		boolean b2 = false;
		
		System.out.println("-------------------");
		System.out.println("Booleans:");
		System.out.println(b1);
		System.out.println(b2);
		
		// byte, short, int, long
		// représentent des nombres entiers
		System.out.println("-------------------");
		System.out.println("Entiers:");
		int i1 = 12345; // 32 bit signé
		System.out.println(i1);
		
		short s1 = 123; // 16 bits signé
		System.out.println(s1);
		
		byte by1 = 12; //8 bits signé
		System.out.println(by1);
		
		long l1 = 123456789; // 64 bits signé
		long l2 = 123456789L;
		System.out.println(l1);
		
		int iDec = 26; // 6 * 10^0 + 2 * 10^1
		int iHex = 0x1a; // a(10) * 16^0 + 1 * 16^1
		int iBin = 0b11010; // 0 * 2^0 + 1 * 2^1 + 0 * 2^2 + 1 * 2^3 + 1 * 2^4
		int iOct = 032; // base octale
				
		// float, double
		/**
		 * Lire:
		 * https://floating-point-gui.de
		 */
		float f1 = 12.54f; // 32 bits signé
		
		double d1 = 14.46; // 64 bits signé
		
		double d2 = 123.4D;
		double d3 = 1.234e2; // notation scientifique
		
		
		// char
		char c1 = 'a'; // caractères Unicode (UTF-16)
		char c2 = '\u0108'; // unicode 'escape'
		
		System.out.println(c2);
				
	
		// Depuis Java 7, pour meilleure lisibilité
		// on peut utiliser des _ entre les chiffres
		int i2 = 100_000_000;				
	}

}
