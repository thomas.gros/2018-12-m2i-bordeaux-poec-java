package com.m2i.training.javase.basics;

/**
 * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html
 * @author thomasgros
 *
 */
public class VariableMain {

	public static void main(String[] args) {
		// variable = nom / etiquette / label
		// utilisé pour simplifier l'accès à de la donnée
		// stockée en mémoire
		String message = "Hello Java";
		
		// En java une variable:
		// - a un type
		// - a un nom (qui commence par une minuscule)
		String unAutreMessage; //déclaration
		unAutreMessage = "Hello again"; // affectation / assign
		
		// System.out.println("Hello Java");
		System.out.println(message);
		
		System.out.println(unAutreMessage);
		
		System.out.println(unAutreMessage);
		
		// une variable peut varier...
		// cad être affectée à une autre donnée
		// forcément du type de la variable
		unAutreMessage = "Coucou !";
		System.out.println(unAutreMessage);	
		
		String encoreUnMessage;
		encoreUnMessage = unAutreMessage;
		
		System.out.println("--------------");
		System.out.println(unAutreMessage); // coucou
		System.out.println(encoreUnMessage); // coucou
		System.out.println("--------------");
		
		unAutreMessage = "j'ai changé";
		
		System.out.println(unAutreMessage); // j'ai changé
		System.out.println(encoreUnMessage); // coucou
	}

}