package com.m2i.training.javase.basics;

public class FizzBuzz {

	public static void main(String[] args) {
		
		// Afficher tous les nombres de 0 à 100 (inclus)
		// Pour chaque nombre:
		// - si le nombre est un multiple de 3 afficher FIZZ <nombre>
		// 	 par exemple: FIZZ 3
		// - si le nombre est un multiple de 5 afficher BUZZ <nombre>
		//	 par exemple: BUZZ 5
		// - si le nombre est un multiple de 3 ET 5 afficher FIZZBUZZ <nombre>
		//	 par exemple: FIZZBUZZ 15
		// - sinon afficher le nombre
		//	 par exemple: 2
		
		for (int i = 0; i <= 100; i++) {
			if(i % 3 == 0 && i % 5 == 0) {
				System.out.println("FIZZBUZZ " + i);
			} else if(i % 3 == 0) {
				System.out.println("FIZZ " + i);
			} else if(i % 5 == 0) {
				System.out.println("BUZZ " + i);
			} else {
				System.out.println(i);
			}
		}		
	}

}
