package com.m2i.training.javase.basics;

public class ControlFlowStatements {

	public static void main(String[] args) {

		// IF_ELSEIF_ELSE
		boolean ilFaitBeau = true;
		
		// if (ilFaitBeau == true) {
		if (ilFaitBeau) {
			System.out.println("il fait beau");
		} else {
			System.out.println("il pleut");
		}
		
		if (ilFaitBeau) {
			System.out.println("ah oui, il fait beau");
		}
		
		System.out.println("on va travailler");

		int temp = 10;
		if (temp < 15) {
			System.out.println("brrr...");
		} else if (temp < 20) {
			System.out.println("il fait doux");
		} else if (temp < 25) {
			System.out.println("on peut sortir");
		} else {
			System.out.println("on pose un RTT");
		}
		
		
		// FOR LOOP
		int compteur = 0; // compteur est locale à main
		for (;;) {
			System.out.println("boucle infinie " + compteur);
			compteur++; // compteur += 1; // compteur = compteur + 1;
			if (compteur == 3) {
				break;
			}
		}
		
		
		for (int compteur2 = 0; compteur2 < 3 ; compteur2++) { // compteur2 est locale à for	
			System.out.println("boucle infinie " + compteur2);
		}

		System.out.println("fin de la boucle");
		
		
		
		// SWITCH
		
		int rating = 3;
		String message;
		
		switch (rating) {
		case 0:
			message = "nul";
			break;
		case 1:
			message = "bof";
			break;
		case 2:
			message = "moyen";
			break;
		case 3:
			message = "bien";
			break;
		case 4:
			message = "très bien";
			break;
		default:
			message = "rating incorrect";
			break;
		}
		
		System.out.println(message);
		
		
		// WHILE
		int compteur3 = 0;
		while (true) {
			System.out.println("boucle while" + compteur3);
			
			compteur3++;
			if (compteur3 == 3) {
				break;
			}
		}
		
		int compteur4 = 3;
		while (compteur4 < 3) {
			System.out.println("boucle while2 " + compteur4);
			compteur4++;
		}
	
		int compteur5 = 3;
		do {
			System.out.println("boucle do while3 " + compteur5);
			compteur5++;
		} while (compteur5 < 3);

	}

}
